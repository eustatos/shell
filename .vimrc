execute pathogen#infect()
execute pathogen#helptags()

filetype plugin indent on

set relativenumber number
set laststatus=2

"file search
set path+=**
set wildmenu
let g:netrw_banner=0
let g:netrw_browser_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

"interface
set nowrap
if !has('gui_running')
  set t_Co=256
endif
set bg=dark
set cc=80
set cursorline
set cursorcolumn
hi LineNr ctermfg=DarkGrey

inoremap jj <ESC>
inoremap " ""<left>
inoremap ' ''<left>
inoremap { {}<left>
inoremap ( ()<left>
inoremap [ []<left>

"autocomplete
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS

"indents
set tabstop=2
set expandtab
set shiftwidth=2
set autoindent
set smartindent

"Snippets
nnoremap ,html :-1read $HOME/.vim/snippets/.skeleton.html<CR>

" Automatically removing all trailing whitespace
autocmd BufWritePre * %s/\s\+$//e
" Highlight unwanted spaces
highlight extraWhiteSpace ctermbg=red guibg=red
" Show trailing whitespace:
match extraWhiteSpace /\s\+$/

" Highlight tab indent
highlight tabIndent  ctermfg=DarkGrey
match tabIndent /\t/
" Show tab indent
set list
set listchars=tab:\┊⋅

" Syntastics settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_html_checkers=['w3, tidy']
let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']

" my rules for code sniffers
let g:syntastic_php_phpcs_args="--report=csv --standard=./phpcs.xml"

" phpDocumentor
source ~/.vim/plugin/php-doc.vim
let g:pdv_cfg_Type = "mixed"
let g:pdv_cfg_Version = ""
let g:pdv_cfg_Author = "Aleksander Astashkin <astashkin@astrovolga.ru>"
let g:pdv_cfg_Copyright = "Copyright (C) 2017 FinTech.Pro. All rights reserved."
let g:pdv_cfg_License = ""
inoremap <C-P> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-P> :call PhpDocSingle()<CR>
vnoremap <C-P> :call PhpDocRange()<CR> )))


" Stop the location list opening automatically
let g:phpqa_open_loc = 0

" config for lighline
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

" config for easyTags
set tags=./tags;
let g:easytags_dynamic_files = 1

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
