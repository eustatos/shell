#!/bin/bash

# Tuning vim

# php indent
# http://www.vim.org/scripts/script.php?script_id=604
if ! [ -d ~/.vim/indent/ ]; then
    mkdir -p ~/.vim/indent;
fi
wget http://www.vim.org/scripts/download_script.php?src_id=2863 \
  -O ~/.vim/indent/php.vim

# install pathogen
# https://github.com/tpope/vim-pathogen
if ! [ -d ~/.vim/autoload/ ]; then
  mkdir -p ~/.vim/autoload;
fi
if ! [ -d ~/.vim/bundle/ ]; then
  mkdir -p ~/.vim/bundle;
fi
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# install fugitive
# https://github.com/tpope/vim-fugitive
if ! [ -d ~/.vim/bundle/vim-fugitive/ ]; then
git clone git://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive/
fi

# install surround
#
if ! [ -d ~/.vim/bundle/vim-surround/ ]; then
git clone git://github.com/tpope/vim-surround.git \
  ~/.vim/bundle/vim-surround/
fi

# install commentary
#
if ! [ -d ~/.vim/bundle/vim-commentary/ ]; then
git clone git://github.com/tpope/vim-commentary.git \
  ~/.vim/bundle/vim-commentary
fi

# install repeat
#
if ! [ -d ~/.vim/bundle/vim-repeat/ ]; then
git clone git://github.com/tpope/vim-repeat.git \
  ~/.vim/bundle/vim-repeat/
fi

# install tabular
#
if ! [ -d ~/.vim/bundle/tabular/ ]; then
git clone git://github.com/godlygeek/tabular.git \
  ~/.vim/bundle/tabular/
fi

# install syntastic
if ! [ -d ~/.vim/bundle/syntastic/ ]; then
git clone --depth=1 https://github.com/vim-syntastic/syntastic.git \
  ~/.vim/bundle/syntastic/
fi

# emmet
#
if ! [ -d ~/.vim/bundle/emmet-vim/ ]; then
git clone https://github.com/mattn/emmet-vim.git \
  ~/.vim/bundle/emmet-vim
fi

# editorconfig
#
if ! [ -d ~/.vim/bundle/editorconfig-vim/ ]; then
git clone https://github.com/editorconfig/editorconfig-vim.git \
  ~/.vim/bundle/editorconfig-vim/
fi

# lightline
#
if ! [ -d ~/.vim/bundle/lightline.vim/ ]; then
 git clone https://github.com/itchyny/lightline.vim \
   ~/.vim/bundle/lightline.vim
 fi

 # matchit
 #
 wget http://www.vim.org/scripts/download_script.php?src_id=8196 \
   -O matchit.zip
 unzip -n matchit.zip -d ~/.vim/
 rm matchit.zip

 # javascript
 #
if ! [ -d ~/.vim/bundle/vim-javascript/ ]; then
 git clone https://github.com/pangloss/vim-javascript.git \
   ~/.vim/bundle/vim-javascript
fi

# php syntax
#
wget http://www.vim.org/scripts/download_script.php?src_id=8651 \
  -O php.tar.gz
tar -zxvf php.tar.gz -C ~/.vim/
rm php.tar.gz

# phpDocumentor
#
wget http://www.vim.org/scripts/download_script.php?src_id=4666 \
  -O ~/.vim/plugin/php-doc.vim

# easymotion
if ! [ -d ~/.vim/bundle/vim-easymotion ]; then
  git clone https://github.com/easymotion/vim-easymotion.git \
    ~/.vim/bundle/vim-easymotion
fi

# prettier
if ! [ -d ~/.vim/bundle/vim-prettier ]; then
  git clone https://github.com/prettier/vim-prettier \
    ~/.vim/bundle/vim-prettier
fi

# react jsx

if ! [ -d ~/.vim/bundle/vim-jsx ]; then
  git clone https://github.com/mxw/vim-jsx.git ~/.vim/bundle/vim-jsx
fi



